set MonoBleedingEdge=C:\Program Files (x86)\Unity\Editor\Data\MonoBleedingEdge


call "Configure KSP Drive and Directory.bat"

rmdir /S /Q "%KSPDIR%_Debug\GameData\KOSM"
rmdir /S /Q "%KSPDIR%_Debug\GameData\KOSMTest\Parts" 

cd .\Release\Plugins
forfiles /s /m *.dll /c "cmd /c """ """%MonoBleedingEdge%\bin\cli.bat""" """%MonoBleedingEdge%\lib\mono\4.0\pdb2mdb.exe""" @path """ "

cd ..\..

xcopy .\Release "%KSPDIR%_Debug\GameData\KOSM" /D /E /C /R /I /K /Y
xcopy .\Tests\saves "%KSPDIR%_Debug\saves" /D /E /C /R /I /K /Y

xcopy .\Tests\Parts "%KSPDIR%_Debug\GameData\KOSMTest\Parts" /D /E /C /R /I /K /Y

%KSDRIVE%
cd %KSPDIR%_Debug
call ksp.exe