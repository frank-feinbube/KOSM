﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using KOSM.Interfaces;
using KOSM.Utility;

namespace KOSM.Game
{
    public class Atmosphere : WorldObject, IAtmosphere
    {
        Body body = null;

        public Atmosphere(World world, Body body)
            : base(world)
        {
            this.body = body;
        }

        public override string Identifier
        {
            get { return "Atmosphere of " + body.Identifier; }
        }

        #region IAtmosphere

        public bool Exists
        {
            get { return body.raw.atmosphere; }
        }

        public double AerobrakingAltitude
        {
            get { return AltitudeWithDensity(0.006); }
        }

        public double Height
        {
            get { return Exists ? body.raw.atmosphereDepth : 0; }
        }

        public double SeaLevelPressure
        {
            get { return body.raw.atmospherePressureSeaLevel * 1000.0; }
        }

        public double SeaLevelTemperature
        {
            get { return body.raw.atmosphereTemperatureSeaLevel; }
        }

        public double SeaLevelDensity
        {
            get { return body.raw.atmDensityASL; }
        }

        public double TemperatureLapseRate
        {
            get { return body.raw.atmosphereTemperatureLapseRate; }
        }

        public double UniversalGasConstant
        {
            get { return PhysicsGlobals.IdealGasConstant; }
        }

        public double MolarMassOfDryAir
        {
            get { return body.raw.atmosphereMolarMass; }
        }

        public double PressureAtAltitude(double altitude)
        {
            return body.raw.GetPressure(altitude) * 1000.0;
        }

        public double TemperatureAtAltitude(double altitude)
        {
            return body.raw.GetTemperature(altitude);
        }

        public double DensityAtAltitude(double altitude)
        {
            return body.raw.GetDensity(PressureAtAltitude(altitude) / 1000.0, TemperatureAtAltitude(altitude));
        }

        public double AltitudeWithDensity(double density)
        {
            // Since 1.0 KSP is modeled after the U.S. Standard Atmosphere which makes it increadibly hard (for me) to reverse calculate the density here.
            // Instead we use a binary search.

            if (density == 0) return this.Height;
            if (density == SeaLevelDensity) return 0;

            return findDensity(density, 0, this.Height);
        }

        #endregion IAtmosphere

        private double findDensity(double density, double from, double to)
        {
            double altitude = Numbers.Interpolate(from, to, 0.5);
            double densityAtAltitude = this.DensityAtAltitude(altitude);
            if (Numbers.CloseEnough(density, 0.0001, densityAtAltitude))
                return altitude;

            if (density > densityAtAltitude)
                return findDensity(density, from, altitude);
            else
                return findDensity(density, altitude, to);
        }
    }
}
