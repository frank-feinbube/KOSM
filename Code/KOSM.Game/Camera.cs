﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

using KOSM.Interfaces;
using KOSM.RelatedWork.CameraTools;

namespace KOSM.Game
{
    public class Camera : WorldObject, ICamera
    {
        FlightCamera raw { get { return FlightCamera.fetch; } }

        List<Action> cameraViews;
        int currentCameraView = 0;

        public Camera(World world)
            : base(world)
        {
            cameraViews = new List<Action>();
            cameraViews.Add(DefaultRocketView);
            cameraViews.Add(FlybyRocketView);
            cameraViews.Add(DefaultMapView);
        }

        # region WorldObject

        public override string Identifier
        {
            get { return "Camera"; }
        }

        # endregion WorldObject

        #region ICamera

        public void BodyBehindRocket(IBody body, IRocket rocket)
        {
            world.LiveDebugLog.Add("Camera hdg is " + raw.camHdg);
            world.LiveDebugLog.Add("Camera pitch is " + raw.camPitch);



            return;
            if (rocket == null)
                return;

            if ((rocket as Rocket).raw == null)
                return;

            raw.transform.position = (v3d(rocket.Position) - v3d(body.Position)).normalized * 1 + v3d(body.Position);
            raw.transform.rotation = Quaternion.LookRotation((rocket as Rocket).raw.transform.position - raw.transform.position, Vector3.up);
        }

        public bool ShowsMap
        {
            get { return MapView.MapIsEnabled; }
        }

        public bool ShowsRocket
        {
            get { return !MapView.MapIsEnabled; }
        }

        public void NextCameraView()
        {
            cameraViews[(currentCameraView++) % cameraViews.Count].Invoke();
        }

        public void DefaultRocketView()
        {
            if (ShowsMap) MapView.ExitMapView();
            // world.PersistentDebugLog.Add("Changed to DefaultRocketView");
            CamTools.instance.RevertCamera();
        }

        public void FlybyRocketView()
        {
            if (ShowsMap) MapView.ExitMapView();
            // world.PersistentDebugLog.Add("Changed to FlybyRocketView");
            CamTools.instance.SetupStationaryCameraWithFlyby(raw.Target.transform, ReferenceModes.Surface, false);
        }

        public void KSCRocketView()
        {
            // (356.5, 134.1, 484.8), auto-zoom, target: rocket
            if (ShowsMap) MapView.ExitMapView();
            CamTools.instance.SetupStationaryCameraWithPosition(new Vector3(356.5f, 134.1f, 484.8f), raw.Target.transform, ReferenceModes.Surface, true);
        }

        public void DefaultMapView()
        {
            if (ShowsRocket) MapView.EnterMapView();
            //  world.PersistentDebugLog.Add("Changed to DefaultMapView");
            CamTools.instance.RevertCamera();
        }

        #endregion ICamera
    }
}
