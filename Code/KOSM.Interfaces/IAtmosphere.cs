﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KOSM.Interfaces
{
    public interface IAtmosphere
    {
        bool Exists { get; }
        double AerobrakingAltitude { get; }
        double Height { get; }
        double SeaLevelPressure { get; }
        double SeaLevelTemperature { get; }
        double SeaLevelDensity { get; }
        double TemperatureLapseRate { get; }
        double UniversalGasConstant { get; }
        double MolarMassOfDryAir { get; }

	    double PressureAtAltitude(double altitude);
	    double TemperatureAtAltitude(double altitude);
	    double DensityAtAltitude(double altitude);

        double AltitudeWithDensity(double density);
    }
}
