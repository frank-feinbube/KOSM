﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using KOSM.Interfaces;
using KOSM.Tasks;
using KOSM.Common;

namespace KOSM.Examples
{
    public class TestScript : IScript
    {
        bool initialized = false;

        List<Action<IWorld>> testCases = null;
        double startingPointInTime = -1.0;

        public void Update(IWorld world)
        {
            if (!initialized)
                initialize(world);

            if (testCases.Count > 0)
                testCases[0](world);
        }

        private void assert(IWorld world, string info, double expected, double actual)
        {
            world.PersistentDebugLog.Add(info + ": expected=" + expected + " was=" + actual + " (difference=" + (actual - expected) + ")");
        }

        private void initialize(IWorld world)
        {
            world.PersistentDebugLog.Clear();

            initialized = true;
            startingPointInTime = world.PointInTime;

            testCases = timeWarpTestCases(world);
        }

        private List<Action<IWorld>> timeWarpTestCases(IWorld world)
        {
            return new List<Action<IWorld>>(){
                    w => {
                        double targetTime = startingPointInTime + 1000;
                        world.LiveDebugLog.Add("waiting till " + targetTime);
                        if (world.OneTickWarpTimeTo(targetTime))
                            return;
                        assert(world, "OneTickWarpTimeTo", targetTime, world.PointInTime);
                        testCases.RemoveAt(0);
                    },
                    w => {
                        double targetTime = startingPointInTime - world.PointInTime + 2000;
                        world.LiveDebugLog.Add("waiting for " + targetTime);
                        if (world.OneTickWarpTime(targetTime))
                            return;
                        assert(world, "OneTickWarpTime", startingPointInTime + 2000, world.PointInTime);
                        testCases.RemoveAt(0);
                    },
                    w => {
                        double targetTime = startingPointInTime + 3000;
                        world.LiveDebugLog.Add("waiting till " + targetTime);
                        if (world.PersistentWarpTimeTo(targetTime))
                            return;
                        assert(world, "PersistentWarpTimeTo", targetTime, world.PointInTime);
                        testCases.RemoveAt(0);
                    }
                };
        }
    }
}
