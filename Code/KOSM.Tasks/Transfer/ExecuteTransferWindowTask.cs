﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using KOSM.Interfaces;
using KOSM.Common;

namespace KOSM.Tasks
{
    public class ExecuteTransferWindowTask : RocketTask
    {
        ITransferWindow transferWindow = null;

        public ExecuteTransferWindowTask(IWorld world, IRocket rocket, ITransferWindow transferWindow)
            : base(world, rocket)
        {
            this.transferWindow = transferWindow;
        }

        public override void Execute(IWorld world, Mission mission)
        {
            if (!rocket.Orbit.IsCircular)
            {
                Details = "Transfer starting orbit need to be circular.";
                mission.PushBefore(this, new CircularizeOrbitTask(world, rocket));
                return;
            }

            transferWindow = rocket.NextTransferWindow(world.PointInTime, rocket.Body, rocket.Orbit.SemiMajorAxis - rocket.Body.Radius, transferWindow.Destination, transferWindow.Aerobraking);
            if (transferWindow.TimeTill > rocket.Orbit.Period)
            {
                Details = "Waiting for Transfer.";
                mission.PushBefore(this, new WarpTask(world, transferWindow.TimeTill, true));
                return;
            }

            if (createManeuver(world, mission) == null)
                return;

            Details = "Transfer requires a DeltaV of " + Format.Speed(transferWindow.EjectionBurnVector.Magnitude) + ".";

            mission.PushAfter(this,
                new ExecuteManeuverTask(world, rocket, (w, r) => r.HasEncounter && r.NextEncounter.Body == transferWindow.Destination),
                new AdjustEncounterTask(world, rocket)
                );

            mission.Complete(world, this);
        }

        private IManeuver createManeuver(IWorld world, Mission mission)
        {
            double timeOfManeuver = world.PointInTime + rocket.Orbit.BodyPrograde.TimeTillDegreesToEquals(transferWindow.EjectionAngle);
            var maneuver = rocket.AddManeuver(timeOfManeuver, transferWindow.EjectionBurnVector);

            if (rocket.NextPlannedEncounter == null)
                Details = "Encounter planning failed! Retrying in next orbital period.";
            else if (rocket.NextPlannedEncounter.Body.Name != transferWindow.Destination.Name)
                Details = rocket.NextPlannedEncounter.Body.Name + " interfered with transfer! Retrying in next orbital period.";
            else
                return maneuver;

            maneuver.Abort();
            mission.PushBefore(this, new WarpTask(world, timeOfManeuver - world.PointInTime + 10, true));
            return null;
        }

        public override string Description
        {
            get { return "Executing transfer from " + transferWindow.Origin + " to " + transferWindow.Destination + "."; }
        }
    }
}
