﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using KOSM.Interfaces;

namespace KOSM.Tasks
{
    public class DeOrbitTask : RocketTask
    {
        protected IOnGroundState destination = null;
        public DeOrbitTask(IWorld world, IRocket rocket, IOnGroundState destination)
            : base(world, rocket)
        {
            this.destination = destination;
        }

        public override void Execute(IWorld world, Mission mission)
        {
            if (rocket.Body != destination.Body)
            {
                mission.PushBefore(this, 
                    new WarpTill(world, rocket, (w, r) => r.Body == destination.Body, (w, r) => 5.0 * destination.Body.Position.Minus(r.Position).Magnitude / r.Orbit.VelocityVector.Magnitude, "Warping to encounter with " + destination.Body.Name + "."),
                    new WaitForRealTimeTask(world, 3) // some time for the game engine to realize that we have a new periapsis
                    );
                return;
            }

            if (rocket.Body.HasAtmosphere)
                executeAtmosphere(world, mission);
            else
                executeVacuum(world, mission);
        }

        private void executeAtmosphere(IWorld world, Mission mission)
        {
            if (rocket.Altitude < rocket.Body.SafeLowOrbitAltitude * 0.2)
            {
                mission.Complete(world, this);
                return;
            }

            if (rocket.Orbit.Periapsis.Altitude > rocket.Body.Atmosphere.AerobrakingAltitude * 1.1)
            {
                if (!rocket.Orbit.IsHyperbolic && (rocket.Orbit.Apoapsis.MovingTowards || rocket.Orbit.Periapsis.Altitude > rocket.Body.LowOrbitAltitude))
                    rocket.AddApoapsisManeuver(rocket.Body.Atmosphere.AerobrakingAltitude + rocket.Body.Radius);
                else
                    rocket.AddPeriapsisManeuver(rocket.Body.Atmosphere.AerobrakingAltitude + rocket.Body.Radius);
                mission.PushBefore(this, new ExecuteManeuverTask(world, rocket));
                return;
            }

            if(rocket.Altitude < rocket.Body.SafeLowOrbitAltitude)
                rocket.SetSteering(rocket.SurfaceRetrograde);

            rocket.Throttle = 0;
            world.OneTickWarpTimeTo(rocket.Orbit.Periapsis.TimeOf);
        }

        bool first = true;
        private void executeVacuum(IWorld world, Mission mission)
        {
            if (rocket.Altitude < rocket.Body.SafeLowOrbitAltitude * 2)
            {
                mission.Complete(world, this);
                return;
            }

            if (rocket.HorizontalSurfaceSpeed < 3)
            {
                rocket.Throttle = 0;
                Details = "Waiting for landing height.";
                return;
            }

            if (rocket.Altitude > rocket.Body.SafeLowOrbitAltitude * 2 && rocket.Orbit.Periapsis.Altitude > rocket.Body.SafeLowOrbitAltitude * 2.1)
            {
                rocket.AddPeriapsisManeuver(rocket.Body.SafeLowOrbitAltitude * 2 + rocket.Body.Radius);
                mission.PushBefore(this, new ExecuteManeuverTask(world, rocket));
                return;
            }

            if (first)
                if (!TurnAndWait(world, rocket.Orbit.Periapsis.TimeTill, rocket.SurfaceRetrograde))
                    return;
            first = false;

            rocket.SetSteering(rocket.SurfaceRetrograde);
            rocket.Throttle = 1;
        }

        public override string Description
        {
            get { return ""; }//"Deorbiting over " + destination + (!rocket.Body.HasAtmosphere ? "" : " with aerobraking") + "."; }
        }
    }
}
