﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KOSM.Utility
{
    public static class Numbers
    {
        public static double Interpolate(double from, double to, double progress)
        {
            return (to - from) * progress + from;
        }

        public static bool CloseEnough(double expected, double errorMargin, double actual)
        {
            return expected * (1 + errorMargin) > actual && expected * (1 - errorMargin) < actual;
        }
    }
}
