﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace KOSM.Windows
{
    public class DebugWindow : ResizableWindow
    {
        List<string> toggled = new List<string>();

        public DebugWindow(int index, string title, double xRatio, double yRatio, double wRatio, double hRatio)
            : base(index, title, xRatio, yRatio, wRatio, hRatio)
        {
        }
        public DebugWindow(ConfigNode node)
            : base(node)
        {
        }

        protected override void buildLayout()
        {
            if (FlightGlobals.ActiveVessel == null)
                return;

            GUI.skin.button.fontSize = 11;
            GUI.skin.button.margin = new RectOffset(1, 1, 1, 1);
            GUI.skin.button.padding = new RectOffset(0, 0, 2, 2);

            render("FlightGlobals.ActiveVessel", FlightGlobals.ActiveVessel);
        }

        protected void render(string path, object obj)
        {
            if (obj is IEnumerable)
            {
                renderList(path, obj as IEnumerable);
                return;
            }

            GUILayout.BeginVertical();

            foreach (var entry in values(obj))
            {
                string name = path + "." + entry.Key;
                GUILayout.BeginHorizontal();
                GUILayout.Label(name + ": ", GUI.skin.label, GUILayout.ExpandWidth(true));
                GUILayout.Label(entry.Value == null ? "null" : entry.Value.ToString(), GUI.skin.label, GUILayout.ExpandWidth(true));
                if (entry.Value != null && (entry.Value.GetType().GetFields().Length > 0 || entry.Value.GetType().GetProperties().Length > 0))
                    if (GUILayout.Button("+", GUI.skin.button))
                        toggle(name);
                GUILayout.EndHorizontal();

                if (toggled.Contains(name))
                    render(name, entry.Value);
            }

            GUILayout.EndVertical();
        }

        protected void renderList(string path, IEnumerable obj)
        {
            int index = 0;
            foreach (var entry in obj)
            {
                string name = path + "[" + index + "]";
                GUILayout.BeginHorizontal();
                GUILayout.Label(name + ": ", GUI.skin.label, GUILayout.ExpandWidth(true));
                GUILayout.Label(entry == null ? "null" : entry.ToString(), GUI.skin.label, GUILayout.ExpandWidth(true));
                if (entry != null && (entry.GetType().GetFields().Length > 0 || entry.GetType().GetProperties().Length > 0))
                    if (GUILayout.Button("+", GUI.skin.button))
                        toggle(name);
                GUILayout.EndHorizontal();

                if (toggled.Contains(name))
                    render(name, entry);

                index++;
            }
        }

        protected void toggle(string path)
        {
            if (!toggled.Contains(path))
                toggled.Add(path);
            else
                toggled.Remove(path);
        }

        private SortedDictionary<string, object> values(object obj)
        {
            SortedDictionary<string, object> result = new SortedDictionary<string, object>();

            foreach (var field in obj.GetType().GetFields())
                if (!result.ContainsKey(field.Name))
                    result.Add(field.Name, field.GetValue(obj));


            foreach (var property in obj.GetType().GetProperties())
                if (property.CanRead && property.GetGetMethod().GetParameters().Count() == 0)
                    if (!result.ContainsKey(property.Name))
                        result.Add(property.Name, property.GetGetMethod().Invoke(obj, null));

            return result;
        }
    }
}
